using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.DependencyInjection;

namespace POS.CobaLagi.Repository
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     if (!optionsBuilder.IsConfigured)
        //     {
        //         optionsBuilder.UseMySQL("server=localhost;database=entity_ef;user=root;password=root");
        //     }
        // }

        // FIX error Unable to resolve service for type '
        public class MysqlEntityFrameworkDesignTimeServices : IDesignTimeServices
        {
            public void ConfigureDesignTimeServices(IServiceCollection serviceCollection)
            {
                serviceCollection.AddEntityFrameworkMySql();
                new EntityFrameworkRelationalDesignServicesBuilder(serviceCollection)
                    .TryAddCoreServices();
            }
        }
        //End

        public DbSet<CategoryEntity> CategoryEntities => Set<CategoryEntity>();
        public DbSet<CustomerEntity> CustomerEntities => Set<CustomerEntity>();
        public DbSet<EmployeeEntity> EmployeeEntities => Set<EmployeeEntity>();
        public DbSet<OrderEntity> OrderEntities => Set<OrderEntity>();
        public DbSet<OrderDetailEntity> OrderDetailEntities => Set<OrderDetailEntity>();
        public DbSet<ProductEntity> ProductEntities => Set<ProductEntity>();
        public DbSet<SupplierEntity> SupplierEntities => Set<SupplierEntity>();
    }

}





