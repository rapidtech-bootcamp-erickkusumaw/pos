using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace POS.CobaLagi.Repository
{
    [Table("tbl_products")]
    public class ProductEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("product_name")]
        public string ProductName { get; set; }

        [Column("supplier_id")]
        public int SupplierId { get; set; }
        public SupplierEntity Supplier { get; set; }

        [Column("category_id")]
        public int CategoryId { get; set; }
        public CategoryEntity Category { get; set; }

        [Column("quantity_per_unit")]
        public String QuantityPerUnit { get; set; }

        [Column("unit_price")]

        public double UnitPrice { get; set; }

        [Column("units_in_stock")]
        public int UnitsInStock { get; set; }

        [Column("units_on_order")]
        public int UnitsOnOrder { get; set; }

        [Column("reorder_level")]
        public int RecorderLevel { get; set; }

        [Column("discontinued")]
        public Boolean Discountinued { get; set; }

        public ICollection<OrderDetailEntity> OrderDetails { get; set; }
    }
}