using POS.CobaLagi.Repository;
using Microsoft.EntityFrameworkCore;
using POS.CobaLagi.ViewModel;
namespace POS.CobaLagi.Service
{
    public class CustomerService
    {
        private void ModelToEntity(CustomerModel model, CustomerEntity entity)
        {
            entity.CompanyName = model.CompanyName;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
        }

        private readonly ApplicationDbContext _context;
        public CustomerService(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<CustomerEntity> GetAll() => _context.CustomerEntities.ToList();

        public List<CustomerEntity> GetIncludeOrders() => _context.CustomerEntities.Include(s => s.Orders).ToList();

        public CustomerEntity GetById(int? id) => _context.CustomerEntities.Find(id);

        public CustomerEntity GetByIdIncludeOrders(int? id) => _context.CustomerEntities.Include(s => s.Orders).FirstOrDefault(s => s.Id == id);

        public void Create(CustomerModel data)
        {
            var entity = new CustomerEntity();
            ModelToEntity(data, entity);
            _context.CustomerEntities.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var entity = _context.CustomerEntities.Find(id);
            _context.CustomerEntities.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(CustomerModel data)
        {
            var entity = _context.CustomerEntities.Find(data.Id);
            ModelToEntity(data, entity);
            _context.CustomerEntities.Update(entity);
            _context.SaveChanges();
        }
    }
}