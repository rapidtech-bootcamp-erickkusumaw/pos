using System.Globalization;
using Microsoft.EntityFrameworkCore;
using POS.CobaLagi.Repository;
using POS.CobaLagi.ViewModel;
using ApplicationDbContext = POS.CobaLagi.Repository.ApplicationDbContext;

namespace POS.CobaLagi.Service
{

    public class SupplierService
    {

        public void ModelToEntity(SupplierModel model, SupplierEntity entity)
        {

            entity.CompanyName = model.CompanyName;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
            entity.HomePage = model.Homepage;
        }


        private readonly ApplicationDbContext _context;
        public SupplierService(ApplicationDbContext context)
        {

            _context = context;

        }

        public List<SupplierEntity> GetAll() => _context.SupplierEntities.ToList();

        public List<SupplierEntity> GetIncludeProduct() => _context.SupplierEntities.Include(s => s.Products).ToList();
        public SupplierEntity GetById(int? id) => _context.SupplierEntities.Find(id);

        public SupplierEntity GetByIdIncludeProducts(int? id) => _context.SupplierEntities.Include(s => s.Products).FirstOrDefault(s => s.Id == id);

        public void Create(SupplierModel data)
        {

            var entity = new SupplierEntity();
            ModelToEntity(data, entity);
            _context.SupplierEntities.Add(entity);
            _context.SaveChanges();

        }
        public void Delete(int? id)
        {
            var entity = _context.SupplierEntities.Find(id);
            _context.SupplierEntities.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(SupplierModel data)
        {
            var entity = _context.SupplierEntities.Find(data.Id);
            ModelToEntity(data, entity);
            _context.SupplierEntities.Update(entity);
            _context.SaveChanges();
        }
    }
}