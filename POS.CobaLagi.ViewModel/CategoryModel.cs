using System.ComponentModel.DataAnnotations;

namespace POS.CobaLagi.ViewModel
{
    public class CategoryModel
    {
        public int Id { get; set; }

        [Required]
        public string CategoryName { get; set; }

        [Required]
        public string Description { get; set; }
        public string? Picture { get; set; }
    }
}
