
using System.ComponentModel.DataAnnotations;

namespace POS.CobaLagi.ViewModel
{
    public class CustomerModel
    {
        public int Id { get; set; }

        [Required]
        public String CompanyName { get; set; }

        [Required]
        public String ContactName { get; set; }

        [Required]
        public String ContactTitle { get; set; }

        [Required]
        public String Address { get; set; }

        [Required]
        public String City { get; set; }

        [Required]
        public String Region { get; set; }

        [Required]
        public String PostalCode { get; set; }

        [Required]
        public String Country { get; set; }

        [Required]
        public String Phone { get; set; }

        [Required]
        public String Fax { get; set; }
    }
}