using System.ComponentModel.DataAnnotations;


namespace POS.CobaLagi.ViewModel
{
    public class EmployeeModel
    {
        public int Id { get; set; }

        [Required]
        public String LastName { get; set; }

        [Required]
        public String FirstName { get; set; }

        [Required]
        public String Title { get; set; }

        [Required]
        public String TitleOfCourtesy { get; set; }

        [Required]
        public DateOnly BirthDate { get; set; }

        [Required]
        public DateOnly HireDate { get; set; }

        [Required]
        public String Address { get; set; }

        [Required]
        public String City { get; set; }

        [Required]
        public String Region { get; set; }

        [Required]
        public String PostalCode { get; set; }

        [Required]
        public String Country { get; set; }

        [Required]
        public String HomePhone { get; set; }

        [Required]
        public String Extension { get; set; }

        [Required]
        public String Notes { get; set; }

        [Required]
        public String ReportsTo { get; set; }
    }
}