using Microsoft.AspNetCore.Mvc;
using POS.CobaLagi.Service;
using ApplicationDbContext = POS.CobaLagi.Repository.ApplicationDbContext;
using POS.CobaLagi.ViewModel;

namespace POS.CobaLagi.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly CategoryService _service;

        public CategoryController(ApplicationDbContext context)
        {
            _service = new CategoryService(context);
        }

        [HttpGet]
        public ActionResult Index()
        {
            var Data = _service.GetIncludeProducts();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Detail(int id)
        {
            var Data = _service.GetByIdIncludeProducts(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpGet]
        public IActionResult CreateModal()
        {
            return PartialView("_Create");
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var Data = _service.GetById(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _service.Delete(id);
            return Redirect("/Category");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("CategoryName, Description")] CategoryModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Create(request);
                return Redirect("/Category");
            }
            return View(request);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, CategoryName, Description")] CategoryModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("/Category");
            }
            return View("Edit", request);
        }
    }
}