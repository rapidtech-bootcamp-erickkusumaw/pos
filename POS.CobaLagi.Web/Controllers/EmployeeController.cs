using Microsoft.AspNetCore.Mvc;
using POS.CobaLagi.Repository;
using POS.CobaLagi.Service;
using POS.CobaLagi.ViewModel;

namespace POS.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly EmployeeService _service;

        public EmployeeController(ApplicationDbContext context)
        {
            _service = new EmployeeService(context);
        }

        [HttpGet]
        public ActionResult Index()
        {
            var Data = _service.GetIncludeOrders();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Detail(int id)
        {
            var Data = _service.GetByIdIncludeOrders(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Modal()
        {
            return PartialView("_Create");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var Data = _service.GetById(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _service.Delete(id);
            return Redirect("/Employee");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension, Notes, ReportsTo")] EmployeeModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Create(request);
                return Redirect("/Employee");
            }
            return View(request);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Extension, Notes, ReportsTo")] EmployeeModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("/Employee");
            }
            return View("Edit", request);
        }
    }
}