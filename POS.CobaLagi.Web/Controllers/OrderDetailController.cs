using Microsoft.AspNetCore.Mvc;
using POS.CobaLagi.Repository;
using POS.CobaLagi.Service;
using POS.CobaLagi.ViewModel;

namespace POS.Web.Controllers
{
    public class OrderDetailController : Controller
    {
        private readonly OrderDetailService _service;

        public OrderDetailController(ApplicationDbContext context)
        {
            _service = new OrderDetailService(context);
        }

        [HttpGet]
        public ActionResult Index()
        {
            var Data = _service.GetAll();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Detail(int id)
        {
            var Data = _service.GetById(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ModalAdd()
        {
            return PartialView("_Create");
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            var Data = _service.GetById(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _service.Delete(id);
            return Redirect("/OrderDetail");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("OrderId, ProductId, UnitPrice, Quantity, Discount")] OrderDetailModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Create(request);
                return Redirect("/OrderDetail");
            }
            return View(request);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, OrderId, ProductId, UnitPrice, Quantity, Discount")] OrderDetailModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("/OrderDetail");
            }
            return View("Edit", request);
        }
    }
}