using Microsoft.AspNetCore.Mvc;
using POS.CobaLagi.Repository;
using POS.CobaLagi.Service;
using POS.CobaLagi.ViewModel;

namespace POS.CobaLagi.Web.Controllers
{

    public class ProductController : Controller
    {
        private readonly ProductService _service;

        public ProductController(ApplicationDbContext context)
        {

            _service = new ProductService(context);

        }

        [HttpGet]
        public ActionResult Index()
        {
            var Data = _service.GetIncludeOrderDetails();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddProductModal()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("ProductName, SupplierId, CategoryId, QuantityPerUnit, UnitPrice, UnitsInStock, UnitsOnOrder, RecorderLevel, Discountinued")] ProductModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Create(request);
                return Redirect("/Product");
            }
            return View(request);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            var Data = _service.GetById(id);
            return View(Data);
        }

        [HttpPost]
        public IActionResult Update([Bind("Id, ProductName, SupplierId, CategoryId, QuantityPerUnit, UnitPrice, UnitsInStock, UnitsOnOrder, RecorderLevel, Discountinued")] ProductModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("/Product");
            }
            return View("Edit", request);
        }


        [HttpGet]
        public IActionResult Delete(int id)
        {
            _service.Delete(id);
            return Redirect("/Product");
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var Data = _service.GetByIdIncludeOrderDetails(id);
            return View(Data);
        }
    }
}