using Microsoft.AspNetCore.Mvc;
using POS.CobaLagi.Repository;
using POS.CobaLagi.Service;
using POS.CobaLagi.ViewModel;

namespace POS.CobaLagi.Web.Controllers
{

    public class SupplierController : Controller
    {

        private readonly SupplierService _service;


        public SupplierController(ApplicationDbContext context)
        {

            _service = new SupplierService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetIncludeProduct();
            return View(data);
        }


        [HttpGet]
        public IActionResult CreateSupplier()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreateSupplierModal()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, Homepage")] SupplierModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Create(request);
                return Redirect("/Supplier");
            }
            return View(request);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _service.Delete(id);
            return Redirect("/Supplier");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var Data = _service.GetById(id);
            return View(Data);
        }


        [HttpPost]
        public IActionResult Update([Bind("Id, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, Homepage")] SupplierModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("/Supplier/List");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var Data = _service.GetByIdIncludeProducts(id);
            return View(Data);
        }
    }


}